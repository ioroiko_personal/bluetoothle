﻿using Plugin.BluetoothLE;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TesterBLE
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private bool _isScanning = false;
        private IDevice currDev = null;
        public bool IsScanning 
        {
            get { return _isScanning; }
            set
            {
                _isScanning = value;
                ManageScanStatus(_isScanning);
            }
        }
        public MainPage()
        {
            InitializeComponent();
            RequestPermissions().ContinueWith((arg)=> {
                Init();
            });
        }

        public async Task<bool> RequestPermissions()
        {
            bool ret = true;
            IPermissions permission = Plugin.Permissions.CrossPermissions.Current;
            try
            {
                //var status = await permission.RequestPermissionsAsync(Permission.Location);

                Device.BeginInvokeOnMainThread(async () =>
                {
                    await Plugin.Permissions.CrossPermissions.Current.RequestPermissionsAsync(new Permission[] { Permission.Location, Permission.LocationAlways, Permission.LocationWhenInUse });
                    int a = 0;
                
                });
                
            }

            catch (Exception e)
            {
                var ecc = e;
            }

            return ret;
        }

        IAdapter ble = null;
        private void Init()
        {
            ble = CrossBleAdapter.Current;
        }

        private void Log(string method, string message)
        {
            string line = string.Format("[{0}]{1}", method, message);
            System.Diagnostics.Debug.WriteLine(line);
        }

        private void ManageScanStatus(bool isScanning)
        {
            Device.BeginInvokeOnMainThread(() => {
                aiScanning.IsVisible = isScanning;
            
            });
        }

        async Task Setup(bool connect)
        {
            devName = currDev.Name;
            currDev = await CrossBleAdapter
                .Current
                .ScanUntilDeviceFound(devName)
                .Timeout(new TimeSpan(0,0,15))
                .ToTask();

            if (connect)
                await Connect();
        }

        string devName = "";
        private void bStart_Clicked(object sender, EventArgs e)
        {
            string method = nameof(bStart_Clicked);
            try
            {
                if (!IsScanning)
                {
                    // discover some devices
                    CrossBleAdapter.Current.Scan().Subscribe(scanResult => {

                        bool found = false;
                        string method2 = "ScanSubscribe";
                        string name = scanResult.AdvertisementData.LocalName;
                        Log(method2, "Found device " + name);
                        if (name == null)
                            return;
                        if (scanResult.AdvertisementData.LocalName.Contains("Voi"))
                        {
                            found = true;
                            currDev = scanResult.Device;
                            bConnect.IsEnabled = true;
                            devName = scanResult.AdvertisementData.LocalName;
                        }
                    });

                    IsScanning = true;
                }
            }
            catch (Exception ex)
            {
                Log(method, "Exception occurred! " + e);
                IsScanning = false;
                throw;
            }
        }

        private void bStop_Clicked(object sender, EventArgs e)
        {
            string method = nameof(bStop_Clicked);
            try
            {
                ble.StopScan();
                IsScanning = false;
            }
            catch (Exception ex)
            {
                Log(method, "Exception occurred! " + ex.Message);
                throw(ex);
            }
        }

        private async Task<bool> Connect()
        {
            string method = nameof(Connect);
            int maxAttempt = 20;
            int attempt = 0;
            bool connected = false;
            ConnectionConfig c = new ConnectionConfig() { AndroidConnectionPriority = ConnectionPriority.High, AutoConnect = false };

            while ((!connected) && (attempt < maxAttempt))
            {
                try
                {
                    Log(method, string.Format("Connecting {0}/{1}", attempt + 1, maxAttempt));
                    //currDev.Connect(c);

                    try
                    {
                        await currDev.ConnectWait().Timeout(new TimeSpan(0,0,5)).ToTask();
                    }
                    catch (Exception ex)
                    {
                        Log(method, "Exception occurred, attempt failed! " + ex.Message);
                    }
                    connected = currDev.IsConnected();
                    Log(method, "Connection success: " + connected);
                }
                catch (Exception ec)
                {
                    Log(method, "Exception occurred EC! " + ec.Message);
                    throw;
                }
                if (!connected)
                {
                    attempt++;
                    await Task.Delay(1000);
                }
                else
                {
                    //Connected!
                    bDisconnect.IsEnabled = true;
                    bConnect.IsEnabled = false;
                    lState.TextColor = Color.Green;
                }

            }
            return connected;
        }

        private async Task<bool> Disconnect()
        {
            string method = nameof(Disconnect);
            bool disconnected = currDev.IsDisconnected();
            try
            {
                Log(method, "START");
                Log(method, "Cancelling connection...");
                currDev.CancelConnection();
                Log(method, "Wait for notification...");
                var disc = await currDev.WhenDisconnected().Take(1).ToTask();
                disconnected = currDev.IsDisconnected();
                Log(method, "Disconnected!");
            }
            catch (Exception ex)
            {
                Log(method, "Exception occurred! " + ex.Message);
            }
            Log(method, "END");

            return disconnected;
        }

        private async void bConnect_Clicked(object sender, EventArgs e)
        {
            string method = nameof(bConnect_Clicked);
            try
            {
                await Setup(true);
                bool connected = currDev.IsConnected();
                
            }
            catch (Exception ex)
            {
                Log(method, "Exception occurred! " + ex.Message);
                throw;
            }
        }

        private async void bDisconnect_Clicked(object sender, EventArgs e)
        {
            string method = nameof(bDisconnect_Clicked);
            try
            {
                bool disc = await Disconnect();
                if (disc)
                {
                    bConnect.IsEnabled = true;
                    bDisconnect.IsEnabled = false;
                    lState.TextColor = Color.Red;
                }           
             }
            catch (Exception ex)
            {
                Log(method, "Exception occurred! " + ex.Message);
            }
        }
    }
}
